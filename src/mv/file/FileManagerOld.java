///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package mv.file;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import javafx.event.EventHandler;
//import javafx.scene.Group;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.input.MouseButton;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.layout.Pane;
//import javafx.scene.layout.StackPane;
//import javafx.scene.paint.Color;
//import javafx.scene.shape.Line;
//import javafx.scene.shape.Polygon;
//import javafx.scene.shape.Polyline;
//import javax.json.Json;
//import javax.json.JsonArray;
//import javax.json.JsonNumber;
//import javax.json.JsonObject;
//import javax.json.JsonReader;
//import javax.json.JsonValue;
//import mv.data.DataManager;
//import mv.data.SubRegions;
//import mv.data.subregionPolygons;
//import saf.components.AppDataComponent;
//import saf.components.AppFileComponent;
//
///**
// *
// * @author McKillaGorilla
// */
//public class FileManagerOld implements AppFileComponent {
//     static double n =1;
//     static double movingValueY=-10.0;
//     static double movingValueX=10.0;
//     boolean backgroup= true;
//     Pane sp;
//     
//    @Override
//    public void loadData(AppDataComponent data, String filePath) throws IOException {
//	//n=1;
//        // CLEAR THE OLD DATA OUT
//	DataManager dataManager = (DataManager)data;
//        sp=dataManager.getPane();
//	dataManager.reset();
//	// LOAD THE JSON FILE WITH ALL THE DATA
//	JsonObject json = loadJSONFile(filePath);
//        //set up NUMBER_OF_SUBREGIONS to dataManager
//        int NUMBER_OF_SUBREGIONS =getDataAsInt(json,"NUMBER_OF_SUBREGIONS");
//        
//        dataManager.setNUMBER_OF_SUBREGIONS(NUMBER_OF_SUBREGIONS);
//        JsonArray jsonItemArray= (JsonArray) json.get("SUBREGIONS");
//        //new method
//        Group gp = loadSubR(jsonItemArray,NUMBER_OF_SUBREGIONS);
//        displayMap(data,gp);
//    }
//    
//    
//    
//    public SubRegions loadSubRegions(JsonObject jsonItem) {
//        SubRegions subRegions=new SubRegions();
//        int NUMBER_OF_SUBREGION_POLYGONS= jsonItem.getInt("NUMBER_OF_SUBREGION_POLYGONS");
//        //System.out.println(NUMBER_OF_SUBREGION_POLYGONS);
//        subRegions.setNumberOfSubregionPolygons(NUMBER_OF_SUBREGION_POLYGONS);
//        
////        JsonArray jsonIA = (JsonArray)((JsonArray)(jsonItem.get("SUBREGION_POLYGONS"))).get(0);
//        JsonArray jsonIA = ((JsonArray)jsonItem.get("SUBREGION_POLYGONS"));
//            for (int j = 0; j < jsonIA.size(); j++) {
//               JsonArray temp=(JsonArray)((JsonArray)(jsonItem.get("SUBREGION_POLYGONS"))).get(j);
//                //System.out.println(temp.toString());
//                for (int i = 0; i < temp.size(); i++) {
//            //loadPolygons
//            JsonObject jsonPOLYGONS = temp.getJsonObject(i);
//	    subregionPolygons subregionpolygons = loadPolygons(jsonPOLYGONS);
//                    
//	    subRegions.addSUBREGION_POLYGONS(subregionpolygons);
//                }
//        }
//        
//     //  }
//        return subRegions; 
//    }
//    
//    public subregionPolygons loadPolygons(JsonObject jsonPOLYGONS){
//        subregionPolygons Polygons = new subregionPolygons();
//        Polygons.setX(getDataAsDouble(jsonPOLYGONS,"X"));
//        Polygons.setY(getDataAsDouble(jsonPOLYGONS,"Y"));
////        System.out.println(Polygons.getX()+"||||"+Polygons.getY());
//        //System.out.println(Polygons.toString());
//        return Polygons;
//        
//    }
//    
//    
//    public Pane getPane(){return sp;}
//    
//    
//    public double getDataAsDouble(JsonObject json, String dataName) {
//	JsonValue value = json.get(dataName);
//	JsonNumber number = (JsonNumber)value;
//	return number.bigDecimalValue().doubleValue();	
//    }
//    
//    public int getDataAsInt(JsonObject json, String dataName) {
//        JsonValue value = json.get(dataName);
//        JsonNumber number = (JsonNumber)value;
//        return number.bigIntegerValue().intValue();
//    }
//    
//    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
//    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
//	InputStream is = new FileInputStream(jsonFilePath);
//	JsonReader jsonReader = Json.createReader(is);
//	JsonObject json = jsonReader.readObject();
//	jsonReader.close();
//	is.close();
//	return json;
//    }
//
//    @Override
//    public void saveData(AppDataComponent data, String filePath) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void exportData(AppDataComponent data, String filePath) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void importData(AppDataComponent data, String filePath) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    private Group loadSubR(JsonArray subR, int n) {
//        Group gp = new Group();
//        
//       // JsonArray jsonIA = ((JsonArray)jsonItem.get("SUBREGION_POLYGONS"));
//            for (int i = 0; i < subR.size(); i++) {
//                 JsonObject jsonItem = subR.getJsonObject(i);
//                 JsonArray jsonIA = ((JsonArray)jsonItem.get("SUBREGION_POLYGONS"));
//                for (int j = 0; j < jsonIA.size();j++) {
//                    JsonArray temp=(JsonArray)jsonIA.get(j);
//                    gp.getChildren().add(addPolyG(temp));
//                }
//                //System.out.println(temp.toString());
//            }
//        return gp;
//    }
//
//    private Polygon addPolyG(JsonArray polygons) {
//        Polygon p = new Polygon();
//        for (int i = 0; i < polygons.size(); i++) {
//            p.getPoints().add(getDataAsDouble((JsonObject)polygons.get(i), "X"));
//            p.getPoints().add(-getDataAsDouble((JsonObject)polygons.get(i), "Y"));
//        }
//            p.setFill(Color.GREEN);
//            p.setStroke(Color.BLACK);
//            p.setStrokeWidth(0.1);
//        return p;
//    }
//
//    private void displayMap(AppDataComponent data,Group g) {
//        
//         DataManager dataManager = (DataManager)data;
//         
//          sp.getChildren().add(g);
////         sp.setScaleX(n);
////        sp.setScaleY(n);
//        dataManager.getApp().getWorkspaceComponent().getWorkspace().getStyleClass().add("background_color");
//        dataManager.getApp().getWorkspaceComponent().getWorkspace().getChildren().add(sp);
//        
//        
//        sp.setOnMouseClicked(new EventHandler<MouseEvent>() {
// 
//            @Override
//            public void handle(MouseEvent event) {
//                MouseButton button = event.getButton();
//                if(button==MouseButton.PRIMARY){
//                    zoomIN(g,dataManager,event);
//                }else if(button==MouseButton.SECONDARY){
//                    zoomOut(g,dataManager,event);
//                }
//            }
//        });
//        
//        
//        dataManager.getApp().getGUI().getPrimaryScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent ke) {
//                switch (ke.getCode()) {
//                    case UP:    moving("UP",sp); break;
//                    case DOWN:  moving("DOWN",sp);break;
//                    case LEFT:  moving("LEFT",sp);break;
//                    case RIGHT: moving("RIGHT",sp);break;
//                    case G: showLines(backgroup);break;
//                }
//            }
//
//        });
//         
//    }
//    private void moving(String key,Pane g){
//        switch (key) {
//                    case "UP":    g.setTranslateY(movingValueY+=15); break;
//                    case "DOWN":  g.setTranslateY(movingValueY-=15);break;
//                    case "LEFT":  g.setTranslateX(movingValueX+=15);break;
//                    case "RIGHT": g.setTranslateX(movingValueX-=15);break;
//                }
//    
//    
//    }
//
//    private void showLines(boolean b) {
//        if(b){
//                 backgroup=false;
//                sp.getStyleClass().clear();
//                sp.getStyleClass().add("root_Backgraound_gridLine");
//        }else{
//            sp.getStyleClass().clear();
//                 sp.getStyleClass().add("background_color");
//                 backgroup=true;
//        
//        }
//             }
//    private void zoomIN(Group g,DataManager dataManager,MouseEvent event) {
//                sp.setLayoutX(sp.getLayoutX()+sp.getWidth()/2*(dataManager.getScale()));
//                sp.setLayoutY(sp.getLayoutY()+sp.getHeight()/2*(dataManager.getScale()));
//                dataManager.setScale(dataManager.getScale()*2);
//                sp.setTranslateX(sp.getTranslateX()-(event.getX()-sp.getTranslateX())+dataManager.getApp().getWorkspaceComponent().getWorkspace().getWidth()/2.0-event.getX());
//                sp.setTranslateY(sp.getTranslateY()-(event.getY()-sp.getTranslateY())+dataManager.getApp().getWorkspaceComponent().getWorkspace().getHeight()/2.0-event.getY());
//               }
//
//    private void zoomOut(Group g,DataManager dataManager,MouseEvent event) {
//                dataManager.setScale(dataManager.getScale()/2);
//                sp.setLayoutX(sp.getLayoutX()-sp.getWidth()/2*(dataManager.getScale()));
//                sp.setLayoutY(sp.getLayoutY()-sp.getHeight()/2*(dataManager.getScale()));
//                sp.setTranslateX(event.getX()-(event.getX()-sp.getTranslateX())/2+dataManager.getApp().getWorkspaceComponent().getWorkspace().getWidth()/2.0-event.getX());
//                sp.setTranslateY(event.getY()-(event.getY()-sp.getTranslateY())/2+dataManager.getApp().getWorkspaceComponent().getWorkspace().getHeight()/2.0-event.getY());
//        
//               }
//
//}
