/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
        double x,y;
        
        public double getX(){return x;}
        public double getY(){return y;}
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
         x = dataManager.getGui().getAppPane().getWidth();
         y = dataManager.getGui().getAppPane().getHeight()-((Pane)dataManager.getGui().getAppPane().getTop()).getHeight();
        System.out.println(x);
        System.out.println(y);
        dataManager.setX(x);
        dataManager.setY(y);
	JsonObject json = loadJSONFile(filePath);
        int numOfRegions = getDataAsInt(json, "NUMBER_OF_SUBREGIONS");
        JsonArray subRegions = json.getJsonArray("SUBREGIONS");
        Group gp = loadSubRegions(subRegions, numOfRegions);
        //gp.rotateProperty().set(-90);
        Pane workPane = dataManager.getPane();
        workPane.getChildren().add(gp);
//        workPane.setLayoutY(-y/1.5);
    }
    
    private Group loadSubRegions(JsonArray subRegions, int num){
        Group gp = new Group();
        for (int i = 0; i < num; i++){
            int numOfPolygons = Integer.parseInt(subRegions.getJsonObject(i).get("NUMBER_OF_SUBREGION_POLYGONS").toString());
            for (int j = 0; j < numOfPolygons; j++){
                JsonArray region = subRegions.getJsonObject(i).getJsonArray("SUBREGION_POLYGONS").getJsonArray(j);
                gp.getChildren().add(createPolygon(region)); 
            }
        }
        return gp;
    }
    
    private Polygon createPolygon(JsonArray region){
        Polygon plg = new Polygon();
        for (int i = 0; i < region.size(); i++){
            plg.getPoints().add(x/360*(180.0+getDataAsDouble((JsonObject)region.get(i), "X")));
            plg.getPoints().add(y/180*(90-getDataAsDouble((JsonObject)region.get(i), "Y")));
        }
        plg.setFill(Color.GREEN);
        plg.setStroke(Color.BLACK);
        plg.setStrokeWidth(0.02);
        return plg;
    }
    
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}