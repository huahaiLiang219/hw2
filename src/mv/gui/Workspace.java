/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.data.DataManager;
import mv.file.FileManager;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    Pane pane;
    Node[] lines;
    double width;
    double height;
    public void setHeight(Double h){height=h;}
    public void setWidth(Double w){width=w;}
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        
        //remove new and save button
        ((FlowPane)app.getGUI().getAppPane().getTop()).getChildren().remove(0);
        ((FlowPane)app.getGUI().getAppPane().getTop()).getChildren().remove(1);
        
        
        workspace = new Pane();
        pane = new Pane();
        pane.setPrefSize(100, 100);
        pane.autosize();
//        workspace.setPrefSize(500, 500);
        workspace.getChildren().add(pane);
        lines = new Node[34];
        initStyle();
        createGridLines();
        
          width = app.getGUI().getAppPane().getWidth();
         height = app.getGUI().getAppPane().getHeight()-((Pane)app.getGUI().getAppPane().getTop()).getWidth();
//        width=app.getGUI().getAppPane().getWidth();
//        height=app.getGUI().getAppPane().getHeight()-((Pane)app.getGUI().getAppPane().getTop()).getWidth();
    }

    @Override
    public void reloadWorkspace() {
        pane.getChildren().clear();
        app.getDataComponent().reset();
    }

    @Override
    public void initStyle() {
        workspace.getStyleClass().add("background_color");
//        pane.setStyle("-fx-background-color: red;");
        Rectangle clipRect = new Rectangle(workspace.getWidth(), workspace.getHeight());
        clipRect.heightProperty().bind(workspace.heightProperty());
        clipRect.widthProperty().bind(workspace.widthProperty());
        workspace.setClip(clipRect);
        ((DataManager)app.getDataComponent()).setPane(pane);  
        workspace.setOnMouseClicked(clickedEvent);
        app.getGUI().getPrimaryScene().setOnKeyPressed(keyPressed);
    }
    
    EventHandler<KeyEvent> keyPressed = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent key) {
            if (key.getCode()==KeyCode.UP)
                pane.setTranslateY(pane.getTranslateY()+20);
            else if (key.getCode()==KeyCode.DOWN)
                pane.setTranslateY(pane.getTranslateY()-20);
            else if (key.getCode()==KeyCode.LEFT)
                pane.setTranslateX(pane.getTranslateX()+20);
            else if (key.getCode()==KeyCode.RIGHT)
                pane.setTranslateX(pane.getTranslateX()-20);
            else if (key.getCode()==KeyCode.G)
                setGlobeGridLines();
        }
    };
    
    EventHandler<MouseEvent> clickedEvent = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (e.getButton().equals(MouseButton.PRIMARY)){
                pane.setLayoutX(pane.getLayoutX()+ pane.getWidth()/2 * ((DataManager)app.getDataComponent()).getScaleValue());
                pane.setLayoutY(pane.getLayoutY()+ pane.getHeight()/2 * ((DataManager)app.getDataComponent()).getScaleValue());
                ((DataManager)app.getDataComponent()).setScale(((DataManager)app.getDataComponent()).getScaleValue()*2);
                pane.setTranslateX(pane.getTranslateX() - (e.getX()- pane.getTranslateX()) + workspace.getWidth()/2.0 - e.getX());
                pane.setTranslateY(pane.getTranslateY() - (e.getY()- pane.getTranslateY()) + workspace.getHeight()/2.0 - e.getY());              
            }else if (e.getButton().equals(MouseButton.SECONDARY)){
                ((DataManager)app.getDataComponent()).setScale(((DataManager)app.getDataComponent()).getScaleValue()/2);
                pane.setLayoutX(pane.getLayoutX() - pane.getWidth()/2 * ((DataManager)app.getDataComponent()).getScaleValue());
                pane.setLayoutY(pane.getLayoutY() - pane.getHeight()/2 * ((DataManager)app.getDataComponent()).getScaleValue());
                pane.setTranslateX(e.getX() - (e.getX()-pane.getTranslateX())/2 + workspace.getWidth()/2.0 - e.getX());
                pane.setTranslateY(e.getY() - (e.getY()-pane.getTranslateY())/2 + workspace.getHeight()/2.0 - e.getY());
            } 
        }
    };
    
    public void setGlobeGridLines(){
        ((DataManager)app.getDataComponent()).switchGlobeGridLines();
        if (((DataManager)app.getDataComponent()).getSwitch()){
            pane.getChildren().addAll(lines);

        }else{
            pane.getChildren().removeAll(lines);
            
            
        }
    }
    
    private void createGridLines(){
            double x = app.getGUI().getAppPane().getWidth()/360;
        double y = (app.getGUI().getAppPane().getHeight()- 
                ((Pane)app.getGUI().getAppPane().getTop()).getHeight())/180;
            for (int i = -3; i < 4; i++){
                if (i<0)
                    lines[i+3] = createHorizontalDottedLine((-200-i*6+180)*x, (i*30+90)*y, (200+i*6+180)*x, (90+i*30)*y);
                else if (i>0)
                    lines[i+3] = createHorizontalDottedLine((-200-i*6+180)*x, (i*30+90)*y, (200+i*6+180)*x, (90+i*30)*y);
                else 
                    lines[i+3] = createWhiteLine(-20*x, 90*y, 380*x, 90*y);
            }
            lines[7] = createWhiteLine(180*x, 0, 180*x, 180*y);   
            for (int j = -6; j < 7; j++){
                if (j<0)
                    lines[j+14] = createVerticalDottedLine((j*30+180)*x, 0, y*(j*35+180) , 60*y, x*(180+j*30+5*j) ,120*y, (j*30+180)*x, 180*y);
                else if (j>0)
                    lines[j+13] = createVerticalDottedLine((j*30+180)*x, 0, y*(j*30+5*j+180) , 60*y, x*(180+j*30+5*j) ,120*y, (j*30+180)*x, 180*y);
            
            }
            lines[20] = createWhiteLine((-175+180)*x, (-90+90)*y, (-175+180)*x, (-75+90)*y);
            lines[21] = createWhiteLine((-175+180)*x, (-75+90)*y, (-172+180)*x, (-74+90)*y);
            lines[22] = createWhiteLine((-172+180)*x, (-74+90)*y, (-172+180)*x, (-69+90)*y);
            lines[23] = createWhiteLine((-172+180)*x, (-69+90)*y, (-180+180)*x, (-64+90)*y);
            lines[24] = createWhiteLine((-180+180)*x, (-64+90)*y, (-175+180)*x, (-62+90)*y);
            lines[25] = createWhiteLine((-175+180)*x, (-62+90)*y, (-175+180)*x, (-20+90)*y);
            lines[26] = createWhiteLine((-175+180)*x, (-20+90)*y, (-160+180)*x, (-20+90)*y);
            lines[27] = createWhiteLine((-160+180)*x, (-20+90)*y, (-152+180)*x, (0+90)*y);
            lines[27] = createWhiteLine((-160+180)*x, (-20+90)*y, (-152+180)*x, (0+90)*y);
            lines[28] = createWhiteLine((-152+180)*x, (-0+90)*y, (-155+180)*x, (4+90)*y);
            lines[29] = createWhiteLine((-155+180)*x, (4+90)*y, (-160+180)*x, (-5+90)*y);
            lines[30] = createWhiteLine((-160+180)*x, (-5+90)*y, (-172+180)*x, (-2+90)*y);
            lines[31] = createWhiteLine((-172+180)*x, (-2+90)*y, (-172+180)*x, (60+90)*y);
            lines[32] = createWhiteLine((-172+180)*x, (60+90)*y, (-175+180)*x, (65+90)*y);
            lines[33] = createWhiteLine((-175+180)*x, (65+90)*y, (-175+180)*x, (90+90)*y);     
    }
    
    private Line createWhiteLine(double sx, double sy, double ex, double ey){
        Line line = new Line(sx, sy, ex, ey);
        line.setStroke(Color.WHITE);
        line.setStrokeWidth(1);
        return line;
    }
    
    private Line createHorizontalDottedLine(double sx, double sy, double ex, double ey){
        Line line = new Line(sx, sy, ex, ey);
        line.setStroke(Color.WHITE);
        line.setStyle("-fx-stroke-dash-array: 4 5 4 5;");
        line.setStrokeWidth(1);
        return line;
    }
    
    private CubicCurve createVerticalDottedLine(
            double sx, double sy, double mx, double my, double cx, double cy, double ex, double ey){
        CubicCurve line = new CubicCurve(sx, sy, mx, my, cx, cy, ex, ey);
        line.setStroke(Color.WHITE);
        line.setStyle("-fx-stroke-dash-array: 4 5 4 5;");
        line.setFill(Color.TRANSPARENT);
        line.setStrokeWidth(1);
        return line;
    }
    
}