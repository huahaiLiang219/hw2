/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author admin
 */
public class SubRegions {
    IntegerProperty NUMBER_OF_SUBREGION_POLYGONS=new SimpleIntegerProperty();
    ArrayList<subregionPolygons> SUBREGION_POLYGONS=new ArrayList<>();
    
    public SubRegions(){
    }
    public SubRegions(int n,ArrayList list){
        this.NUMBER_OF_SUBREGION_POLYGONS.set(n);
        this.SUBREGION_POLYGONS=list;
    }
    
    
    public int getNUMBER_OF_SUBREGION_POLYGONS(){return NUMBER_OF_SUBREGION_POLYGONS.get(); }
    public ArrayList<subregionPolygons> getSUBREGION_POLYGONS(){return SUBREGION_POLYGONS;}
    
    
    public void setNumberOfSubregionPolygons(int n){NUMBER_OF_SUBREGION_POLYGONS.set(n);}
    public void addSUBREGION_POLYGONS(subregionPolygons subregionpolygons){SUBREGION_POLYGONS.add(subregionpolygons); }
    
}
