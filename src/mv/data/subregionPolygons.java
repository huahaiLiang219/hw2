/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;


/**
 *
 * @author admin
 */
public class subregionPolygons {
     DoubleProperty x= new SimpleDoubleProperty();
     DoubleProperty y= new SimpleDoubleProperty();
    
     
     
     public subregionPolygons(){
     }
     
     public subregionPolygons(Double paramX,Double paramY){
         x.set(paramX);
         y.set(paramY);
     }
     
     
    public DoubleProperty XProperty(){return x ;}
    public DoubleProperty YProperty(){return y ;}
    
    
    public Double getX(){return x.get() ;}
    public Double getY(){return y.get() ;}
    
    
    
    public void setX(double x){this.x.set(x);}
    public void setY(double y){this.y.set(y);}
}
