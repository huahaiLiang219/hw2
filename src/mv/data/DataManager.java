package mv.data;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import saf.ui.AppGUI;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    SimpleDoubleProperty scaleValue;
    Pane pane;
    boolean gridLineSwitch;
    double x=0;
    double y=0;
        
        public double getX(){return x;}
        public double getY(){return y;}
        public void setX(double wid){x=wid;}
        public void setY(double hei){x=hei;}
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        scaleValue = new SimpleDoubleProperty(1);
        gridLineSwitch = false;
        autoScale();
    }
    
    public void setScale(double value){
        scaleValue.set(value);
    }
    public double getScaleValue(){
        return scaleValue.get();
    }   
    private void autoScale(){
        scaleValue.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                pane.setScaleX(scaleValue.get());
                pane.setScaleY(scaleValue.get());
            }
        });
    }
    
    public AppGUI getGui(){
    return app.getGUI();
    }
    public void setPane(Pane p){
        pane = p;
    }
    
    public void switchGlobeGridLines(){
        gridLineSwitch = !gridLineSwitch;
    }
    public boolean getSwitch(){
        return gridLineSwitch;
    }
    
    public Pane getPane(){
        return (Pane)app.getWorkspaceComponent().getWorkspace().getChildren().get(0);
    }
    
    public Pane getWorkSpace(){
        return app.getWorkspaceComponent().getWorkspace();
    }
    @Override
    public void reset() {
        scaleValue.set(1);
        pane.setTranslateX(0);
        pane.setTranslateY(0);
        pane.setLayoutX(0);
        pane.setLayoutY(0);
        gridLineSwitch = false;
    }
}